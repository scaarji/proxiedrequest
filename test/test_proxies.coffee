should = require 'should'
async = require 'async'
clone = require 'clone'

Proxies = require '../lib/proxies'

describe 'Proxies', ->
    globalProxies = null
    before ->
        globalProxies = new Proxies [
            'http://125.216.144.199:8080'
            'http://187.115.151.36:8080'
        ]

    it 'accepts list as constructor parameter', ->
        proxies = clone globalProxies
        proxies._list.should.have.length 2

    it 'should accept proxies list and replace list with them', ->
        proxies = clone globalProxies
        proxies.setList [
            'proxy1'
            'proxy2'
            'proxy3'
        ]
        proxies._list.should.have.length 3

    it 'should return proxy', (done) ->
        proxies = clone globalProxies
        namespace = 'default'
        proxies.claimByNamespace namespace, (proxy) ->
            should.exist proxy
            proxies._taken.should.have.length 1
            do done

    it 'should free proxy and make it available if stated so', (done) ->
        proxies = clone globalProxies
        namespace = 'default'
        proxies.claimByNamespace namespace, (proxy) ->
            claimedProxy = proxy

            proxies.freeByNamespace namespace, claimedProxy, false
            proxies._taken.should.have.length 0

            async.times 2, (i, cb) ->
                proxies.claimByNamespace namespace, (proxy) -> cb null, proxy
            , (err, list) ->
                list.should.include claimedProxy
                do done

    it 'if no proxies are available should return one when it is freed', (done) ->
        proxies = clone globalProxies
        namespace = 'default'
        async.times 2, (i, cb) ->
            proxies.claimByNamespace namespace, (proxy) -> cb null, proxy
        , (err, list) ->
            claimedProxy = null
            checkedProxyExistance = false

            proxies.claimByNamespace namespace, (proxy) ->
                claimedProxy = proxy
                checkedProxyExistance.should.eql true
                should.exist proxy
                do done

            setTimeout ->
                should.not.exist claimedProxy
                checkedProxyExistance = true
                proxies.freeByNamespace namespace, list.shift(), false
            , 10

    it 'should free proxy and invalidate it if stated so', (done) ->
        proxies = clone globalProxies
        namespace = 'default'
        proxies.claimByNamespace namespace, (proxy) ->
            claimedProxy = proxy
            proxies.freeByNamespace namespace, proxy, true
            proxies._taken.should.have.length 0
            proxies._invalid.should.have.ownProperty namespace
            proxies._invalid[namespace].should.include claimedProxy

            async.times 1, (i, cb) ->
                proxies.claimByNamespace namespace, (proxy) -> cb null, proxy
            , (err, list) ->
                list.should.not.include claimedProxy
                proxies.claimByNamespace namespace, (proxy) ->
                    proxy.should.not.eql claimedProxy
                    do done

                setTimeout ->
                    proxies.freeByNamespace namespace, item, false for item in list
                , 10

    it 'should clear taken proxies list if there are no available proxies left', (done) ->
        proxies = clone globalProxies
        namespace = 'default'
        async.times 2, (i, cb) ->
            proxies.claimByNamespace namespace, (proxy) ->
                proxies.freeByNamespace namespace, proxy, true
                cb null
        , ->
            proxies._invalid[namespace].should.include proxy for proxy in proxies._list
            proxies.claimByNamespace namespace, (proxy) ->
                should.exist proxy
                proxies._invalid[namespace].should.have.length 0
                do done

