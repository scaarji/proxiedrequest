should = require 'should'
http = require 'http'
url = require 'url'
clone = require 'clone'

ProxiedRequest = require '../lib/proxiedRequest'

describe 'proxiedRequest', ->
    server = null
    proxyServer = null
    before (done) ->
        tries = {}
        server = http.createServer (request, response) ->
            query = url.parse(request.url, true).query or {}

            timeout = query.timeout or 0
            status = query.status or 200
            text = query.text or 'Hello world'

            if query.nthSuccessful? and query.id?
                nthSuccessful = query.nthSuccessful
                id = query.id

                tries[id] ?= 0
                tries[id] += 1

                if tries[id] >= nthSuccessful
                    status = 200
                else
                    status = 500

                timeout = 0 if tries[id] > 1

            setTimeout ->
                response.writeHead parseInt(status, 10),
                    'Content-Type': 'text/plain'
                response.end(text)
            , timeout

        server.listen 8080

        proxyServer = http.createServer (request, response) ->
            parsedUrl = url.parse(request.url)

            proxy_request = http.request
                hostname: parsedUrl.hostname
                port: parsedUrl.port
                path: parsedUrl.path
                header: request.headers
                method: request.method
            , (proxy_response) ->
                response.writeHead proxy_response.statusCode,
                    proxy_response.headers
                proxy_response.on 'data', (chunk) ->
                    response.write chunk, 'binary'
                proxy_response.on 'end', ->
                    response.end()

            request.addListener 'data', (chunk) ->
                proxy_request.write chunk, 'binary'

            request.addListener 'end', ->
                proxy_request.end()

        proxyServer.listen 8088
        do done

    after (done) ->
        server.close()
        proxyServer.close()
        do done

    # @timeout 0

    proxiedRequest = null
    before (done) ->
        proxiedRequest = new ProxiedRequest [
            'http://localhost:8088'
        ]
        proxiedRequest.offset = min: 0, max: 10
        proxiedRequest.maxWait = 20
        proxiedRequest.maxTries = 2
        do done

    describe 'request', ->
        it 'should get url from options if not given implicitly', (done) ->
            proxiedRequest.request
                url: 'http://localhost:8080'
            , (err, res, body) ->
                should.not.exist err
                body.should.eql 'Hello world'
                do done

        it 'should override default options by custom ones', (done) ->
            proxiedRequest.request 'http://localhost:8080?text=Init',
                url: 'http://localhost:8080'
            , (err, res, body) ->
                should.not.exist err
                body.should.eql 'Hello world'
                do done

        it 'should return body of given url if request succeded', (done) ->
            proxiedRequest.request 'http://localhost:8080', (err, res, body) ->
                should.not.exist err
                body.should.eql 'Hello world'
                do done

        it 'should return error if request time exceeds maxWait', (done) ->
            proxiedRequest.request 'http://localhost:8080/?timeout=1000&text=',
                (err, res, body) ->
                    should.exist err
                    should.not.exist body
                    do done

    describe 'safeRequest', ->
        it 'should return response and body if request was successfull',
            (done) ->
                originalUrl = 'http://localhost:8080'
                proxiedRequest.safeRequest originalUrl, (err, res, body) ->
                    should.not.exist err
                    body.should.eql 'Hello world'
                    do done

        it 'should retry request given number of time before giving up',
            (done) ->
                originalUrl = 'http://localhost:8080/?' +
                    'nthSuccessful=2&id=uniqueId'
                proxiedRequest.safeRequest originalUrl, (err, res, body) ->
                    should.not.exist err
                    body.should.eql 'Hello world'
                    do done

        it 'should handle request timeouts correctly', (done) ->
            originalUrl = 'http://localhost:8080/?nthSuccessful=2&' +
                'id=timeoutId&timeout=1000'
            proxiedRequest.safeRequest originalUrl, (err, res, body) ->
                should.not.exist err
                body.should.eql 'Hello world'
                do done

        it 'should give up after number of failed requests', (done) ->
            originalUrl = 'http://localhost:8080?status=500'
            proxiedRequest.safeRequest originalUrl, (err, res, body) ->
                should.exist err
                do done
