userAgentStrings = require '../assets/userAgents.json'
length = userAgentStrings.length
module.exports = ->
    position = Math.round(Math.random() * (length - 1))
    userAgentStrings.slice(position, position + 1).shift()
