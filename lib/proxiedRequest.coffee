request = require 'request'

Proxies = require './proxies'
userAgentsGen = require './userAgents'

round = Math.round
random = Math.random

class ProxiedRequest
    maxTries: 5
    maxWait: 20000
    _jars: {}
    _ns: 'default'
    offset:
        min: 500
        max: 1500
    constructor: (proxiesList) ->
        @proxies = new Proxies proxiesList
        @safeRequest = @safeRequest.bind this
        @request = @request.bind this
    _parseParams: (url, options, cb) ->
        if typeof url isnt 'string'
            if typeof url is 'function' or typeof options isnt 'function'
                throw new Error """
                Invalid usage first argument can not be a function
                """
            unless url.url?
                throw new Error 'Url not given'
            cb = options
            options = url
            url = options.url
        if typeof options is 'function'
            cb = options
            options = {}
        [url, options, cb]
    safeRequest: (url, options, cb) ->
        [url, options, cb] = @_parseParams url, options, cb
        tries = 0
        _request = =>
            @request url, options, (err, res, body) =>
                if err or res.statusCode isnt 200 or not body?.length
                    tries += 1
                    body = null

                    if tries > @maxTries
                        cb new Error('request failed')
                    else
                        timeout = @offset.min +
                            round((@offset.max - @offset.min) * random())
                        setTimeout _request, timeout
                else
                    cb null, res, body
        do _request
    request: (url, options, cb) ->
        [url, options, cb] = @_parseParams url, options, cb
        setTimeout =>
            if options.hide is off
                timeout = null
                finished = no
                req = request url, options, (err, res, body) ->
                    return if finished
                    clearTimeout timeout if timeout
                    finished = yes
                    cb err, res, body
                    res = null
                    body = null

                if @maxWait
                    timeout = setTimeout ->
                        return if finished
                        finished = yes
                        req.abort()
                        cb new Error "Request to #{url} timeouted"
                    , @maxWait
            else
                @proxies.claimByNamespace @_ns, (proxy) =>
                    @_jars[proxy] ?= request.jar()
                    jar = @_jars[proxy]
                    requestOptions =
                        url: url
                        jar: jar
                        proxy: proxy
                        headers:
                            'User-Agent': userAgentsGen()
                            'Accept-Language': 'ru,en-US;q=0.8,en;q=0.6'
                            'Accept': 'text/html,application/xhtml+xml,' +
                                'application/xml;q=0.9,*/*;q=0.8'

                    requestOptions[k] = v for own k, v of options

                    timeout = null
                    finished = no
                    req = request requestOptions, (err, res, body) =>
                        return if finished
                        clearTimeout timeout if timeout
                        finished = yes
                        @proxies.freeByNamespace @_ns, proxy, yes
                        cb err, res, body
                        res = null
                        body = null

                    if @maxWait
                        timeout = setTimeout =>
                            return if finished
                            finished = yes
                            req.abort()
                            @proxies.freeByNamespace @_ns, proxy, yes
                            cb new Error "request to #{url} timeouted"
                        , @maxWait
        , @offset.min + round((@offset.max - @offset.min) * random())

module.exports = ProxiedRequest
