class Proxies
    _list: []
    _taken: []
    _queue: []
    _invalid: {}
    constructor: (list) ->
        @setList list

    setList: (list) ->
        return unless list instanceof Array
        @_list = list

    claimByNamespace: (namespace, cb) ->
        @_invalid[namespace] ?= []
        if @_invalid[namespace].length is @_list.length
            @_invalid[namespace] = []
        process.nextTick =>
            list = []
            for proxy in @_list
                unless proxy in @_invalid[namespace] or proxy in @_taken
                    list.push proxy
            unless list and list.length > 0
                @_queue.push
                    namespace: namespace
                    cb: cb
            else
                proxy = list[Math.round(Math.random() * (list.length - 1))]
                @_taken.push proxy
                cb proxy

    freeByNamespace: (namespace, proxy, invalidate = false) ->
        return unless proxy in @_list
        # invalidate proxy
        @_invalid[namespace].push proxy if invalidate

        # remove proxy from taken list
        index = @_taken.indexOf proxy
        @_taken.splice index, 1 if index isnt -1

        if @_queue.length > 0
            for item, i in @_queue when proxy not in @_invalid[item.namespace]
                @_queue.splice i, 1
                item.cb proxy
                break

        do @_checkQueue

    _checkQueue: ->
        process.nextTick =>
            for namespace of @_invalid
                if @_invalid[namespace].length is @_list.length
                    @_invalid[namespace] = []
                    return unless @_queue.length
                    for item, i in @_queue when item?.namespace is namespace
                        @_queue.splice i, 1
                        @claimByNamespace item.namespace, item.cb

module.exports = Proxies
